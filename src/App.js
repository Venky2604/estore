import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";
import HomePage from './components/HomePage';
import CommonContainer from './components/CommonContainer';
import ShopPage from './components/ShopPage';
import CartPage from './components/CartPage';
import ProductDetail from './components/ProductDetail';

class App extends Component {
	render() {
		return ( 
			<Router>
				<CommonContainer>
					<Switch>
						<Route exact path="/" component={HomePage}/>
						<Route path="/shop" component={ShopPage}/>
						<Route path="/cart" component={CartPage}/>
						<Route path="/product" component={ProductDetail}/>
					</Switch>
				</CommonContainer>
			</Router>
		);
	}
}

export default App;