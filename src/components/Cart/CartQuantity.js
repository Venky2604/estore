import React, { Component } from "react";

class Cartquantity extends Component{

    constructor(props){
        super(props);
        this.state = {
            quantity: props.quantity
        };
        this.inc = this.inc.bind(this);
        this.dec = this.dec.bind(this);
    }

    inc(){
        this.setState(function(prevState, props){
            return {
                quantity: prevState.quantity < props.maxQuantity ? prevState.quantity + 1 : prevState.quantity
            }
        })
    }

    dec(){
        this.setState(function(prevState, props){
            return {
                quantity: prevState.quantity > 1 ? prevState.quantity - 1 : prevState.quantity
            }
        })
    }

    render(){
        return (
            <div className="js-qty">
                <button type="button" className="js-qty__adjust js-qty__adjust--minus icon-fallback-text" onClick={this.dec}>
                    <span className="fallback-text">−</span>
                </button>
                <input type="text" className="js-qty__num" value={this.state.quantity} aria-label="quantity" readOnly />
                <button type="button" className="js-qty__adjust js-qty__adjust--plus icon-fallback-text" onClick={this.inc}>
                    <span className="fallback-text">+</span>
                </button>
            </div>
        )
    }
}

export default Cartquantity;