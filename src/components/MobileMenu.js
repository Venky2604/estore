import React, {Component} from "react";
import logo from "../assets/images/flatly_logo.png";
import { Link } from "react-router-dom";

class Mobilemenu extends Component{

    constructor(props){
        super(props);
        this.state = {
            open: false
        };
        this.menu = this.menu.bind(this);
    }
    
    menu(){
        this.setState(function(prevState){
            return {
                open: !prevState.open
            }
        })
    }

    render(){
        let { open } = this.state; 
        return (
            <React.Fragment>
                <header className="mobilemenu stickyheader">
                    <div>
                        <Link to="/">
                            <img src={ logo } alt="logo" className="mobilelogo"/>
                        </Link>
                    </div>
                    <div onClick={this.menu}>
                        <i className={`icon-${open ? "close" : "menu"} icons`} aria-hidden="true"></i>
                    </div>
                </header>
                {
                    open &&
                    <ul className="mobile-sidemenu list-group">
                        <div className="sidemenu-background">
                            
                        </div>
                        <li className="awemenu-item">
                            <i className="icon-home pr10" aria-hidden="true"></i>
                            <Link to="/">Home</Link>
                        </li>
                        <li className="awemenu-item">
                            <i className="icon-bag pr10" aria-hidden="true"></i>
                            <Link to="/shop">Shop</Link>
                        </li>
                        <li className="awemenu-item">
                        <i className="icon-mustache pr10" aria-hidden="true"></i>
                            <Link to="/shop">Men</Link>
                        </li>
                        <li className="awemenu-item">
                            <i className="icon-user-female pr10" aria-hidden="true"></i>
                            <Link to="/shop">Women</Link>
                        </li>
                        <li className="awemenu-item">
                            <i className="icon-user pr10" aria-hidden="true"></i>
                            <Link to="/shop">Kids</Link>
                        </li>
                        <li className="awemenu-item">
                            <i className="icon-basket pr10" aria-hidden="true"></i>
                            <Link to="/cart">Cart</Link>
                        </li>
                        <li className="awemenu-item">
                            <i className="icon-logout pr10" aria-hidden="true"></i>
                            <Link to="/cart">Logout</Link>
                        </li>
                    </ul>
                }
            </React.Fragment>
        )
    }
}

export default Mobilemenu;