import React from "react";
import HeroSlider from "./Home/HeroSilder";
import Product from "./Product/Product";
import { Link } from "react-router-dom";

//images import 
import fashion01 from "../assets/images/fashion-01.jpg";
import fashion02 from "../assets/images/fashion-02.jpg";
import fashion03 from "../assets/images/fashion-03.jpg";
import fashion04 from "../assets/images/fashion-04.jpg";
import banner01 from "../assets/images/slide3-02.jpg";
import banner02 from "../assets/images/slide3-01.jpg";
import category01 from  "../assets/images/banner_collection6.jpg"
import category02 from  "../assets/images/banner_collection4.jpg"
import category03 from  "../assets/images/banner_collection5.jpg"


const HomePage = () => (
    <div>
        <IndexView>
            <HeroSlider banners={[banner01, banner02]}/>
        </IndexView>
        <IndexView divStyle={ {marginTop: `-50px`} }>
            <section className="section container-fluid">
                <div className="br-card-categories--2 row">
                    <div className="col-md-4 col-sm-6 col-xs-12 inner-title br-card-categories__item">
                        <div className="br-card__media">
                            <div className="engoc_hover_img">
                                <Link to="/shop">
                                    <img src={category01} alt="" />
                                </Link>
                                <div className="br-card__main">
                                    <h2 className="br-card-categories__title text-uppercase">Shop
                                        <br />
                                        <span>Fashion</span>
                                    </h2>
                                    <Link to="/shop" className="btn btn-primary">Shop now</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 col-sm-6 col-xs-12 inner-title br-card-categories__item">
                        <div className="br-card__media">
                            <div className="engoc_hover_img">
                                <Link to="/shop">
                                    <img src={category02} alt="" />
                                </Link>
                                <div className="br-card__main">
                                    <h2 className="br-card-categories__title text-uppercase">Shop
                                        <br />
                                        <span>Clothes</span>
                                    </h2>
                                    <Link to="/shop" className="btn btn-primary">Shop now</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 col-sm-6 col-xs-12 inner-title br-card-categories__item">
                        <div className="br-card__media">
                            <div className="engoc_hover_img">
                                <Link to="/shop">
                                    <img src={category03} alt="" />
                                </Link>
                                <div className="br-card__main">
                                    <h2 className="br-card-categories__title text-uppercase">shop
                                        <br />
                                        <span>Trends</span>
                                    </h2>
                                    <Link to="/shop" className="btn btn-primary">shop now</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </IndexView>
        <IndexView>
            <section className="section pt-0 engoc_no_rtl pb0">
                <div className="container-fluid">
                    <h2 className="br-title text-center">New Arrivals</h2>
                    <p className="br-description">Browse our website for the hottest items in the marketplace now.</p>
                    <div className="row">
                        <div className="col-md-3 col-sm-6 col-tn-6">
                            <Product 
                                title="Product 01" 
                                images={[fashion01, fashion02] }
                                price={10920}
                                strickedPrice={20920}
                            />
                        </div>
                        <div className="col-md-3 col-sm-6 col-tn-6">
                            <Product 
                                title="Product 01" 
                                images={[fashion02, fashion03] }
                                price={10920}
                                strickedPrice={20920}
                            />
                        </div>
                        <div className="col-md-3 col-sm-6 col-tn-6">
                            <Product 
                                title="Product 01" 
                                images={[fashion03, fashion04] }
                                price={10920}
                                strickedPrice={20920}
                            />
                        </div>
                        <div className="col-md-3 col-sm-6 col-tn-6">
                            <Product 
                                title="Product 01" 
                                images={[fashion04, fashion01] }
                                price={10920}
                                strickedPrice={20920}
                            />
                        </div>
                        <div className="col-md-3 col-sm-6 col-tn-6">
                            <Product 
                                title="Product 01" 
                                images={[fashion01, fashion02] }
                                price={10920}
                                strickedPrice={20920}
                            />
                        </div>
                        <div className="col-md-3 col-sm-6 col-tn-6">
                            <Product 
                                title="Product 01" 
                                images={[fashion03, fashion01] }
                                price={10920}
                                strickedPrice={20920}
                            />
                        </div>
                        <div className="col-md-3 col-sm-6 col-tn-6">
                            <Product 
                                title="Product 01" 
                                images={[fashion04, fashion02] }
                                price={10920}
                                strickedPrice={20920}
                            />
                        </div>
                        <div className="col-md-3 col-sm-6 col-tn-6">
                            <Product 
                                title="Product 01" 
                                images={[fashion03, fashion01] }
                                price={10920}
                                strickedPrice={20920}
                            />
                        </div>
                    </div>
                    {/* <div className="text-center">
                        <a className="btn btn-default">shop more</a>
                    </div> */}
                </div>
            </section>
        </IndexView>
    </div>
);

const IndexView = ({ divStyle, children}) => (
    <div className="shopify-section index-section">
        <div style={divStyle}>
            { children }
        </div>
    </div>
);

export default HomePage;