import React from 'react';
import Slider from '../Slider';
import HeroSliderItem from './HeroSlideItem';
import { Link } from "react-router-dom";

const HeroSlider = ({ banners }) => (
    <section className="engoc_no_rtl">   
        <Slider 
            slideCount={1}
            slideWidth={100}
            widthMetric={`%`}
            containerClasses={["js-slide-banner-full-1", "br-slick-white" ,"br-slick-animation"]}
        >
            <HeroSliderItem bgImage={banners[0]} textAlign={`center`}>
                <div className="row">
                    <div className="col-md-12">
                        <span className=" uppercase white" style={{textAlign: `center`, color:` #ffffff`, fontSize: `20px`, marginBottom: `10px`}}>Lookbook fashion 2018</span>
                        <h2 className="font-secondary white bn1">Spring & Summer</h2>
                        <Link to="/shop" className="btn btn-primary">shop now</Link>
                    </div>
                </div>
            </HeroSliderItem>
            <HeroSliderItem bgImage={banners[1]} textAlign={`center`}>
                <div className="row">
                    <div className="col-md-6">
                        <span className=" uppercase white" style={{textAlign: `left`, color: `#000000`, fontSize: `20px`, marginBottom: `10px`}}>Collection Item</span>
                        <h2 className="font-secondary white" style={{textAlign: `left`, color: `#242626`, fontSize: `80px`, marginBottom: `30px`}}>Short spring Flatly</h2>
                        <Link to="/shop" className="btn btn-default">view more</Link>
                        <Link to="/shop" className="btn btn-primary">shop now</Link>
                    </div>
                </div>
            </HeroSliderItem>
        </Slider>
    </section>
);

export default HeroSlider;