import React from 'react';

const HeroSliderItem = ({ children, bgImage, textAlign }) => (
    <div className="slide-banner-full">
        <div className="br-background bg-slide-5" style={{backgroundImage: `url('${bgImage}')`}}>
            <div className="slide-banner-full-content slide-banner-slide-5" style={{ textAlign }}>
                <div className="container">
                    { children }
                </div>
            </div>
        </div>
    </div>
);

export default HeroSliderItem;