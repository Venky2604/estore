import React from "react";
import { Link } from "react-router-dom";

const Product = ({
    title,
    images,
    price,
    strickedPrice
}) => (
    <div className="br-product br-product--grid  engoj_grid_parent engoj-product-handle" data-handle="bluisemod-amet">
        <div className="br-product__media">
            <div className="br-product__thumb">
                <Link to="product">
                    <img src={ images[0] } alt={title} />
                    <img className="engoj_find_img engoc_product-image-last" src={ images[1] } alt={title} />
                </Link>
            </div>   
            <div className="br-product__label-wrapper">
                {/* <span className="br-product__label new engoc_new_label">New</span> */}
                {/* <span className="br-product__label sale engoc_sale_label">- 28% </span> */}
            </div>
            <div className="br-product__action engoc-product-action">
                <div className="tb">
                    <div className="table-cell-center engoc-action-quickview">
                        <a className="btn quickview btn-quickview engoj_btn_quickview"  data-id="bluisemod-amet" data-toggle="tooltip"
                            data-placement="top" title="" data-original-title="Quickview">
                            <span className="sr-only">Quickview</span>
                            <i className="icon-eye icons"></i>
                        </a>
                    </div>
                    <div className="table-cell-center engoc-action-addcart">
                        <form method="post" action="/cart/add" className="add-to-cart">
                            <button type="submit" className="enj-add-to-cart-btn btn colorwhite" data-toggle="tooltip" data-placement="top"
                                title="Add to Cart" data-original-title="Add to Cart">  
                                <span>Add to Cart</span>
                            </button>
                        </form>
                    </div>
                    <div className="table-cell-center engoc-action-wishlist">
                        <a className="add_to_wishlist wishlist  awe-button product-quick-whistlist btn" href="/account/login" data-toggle="tooltip" data-placement="top"
                            title="" data-original-title="Add to Wishlist">
                            <span className="sr-only">Add to Wishlist</span>
                            <i className="icon-heart icons"></i>
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <div className="br-product__main text-center">
            <h4 className="br-product__name">
                <a href="/products/bluisemod-amet">{ title }</a>
            </h4>
            <span className="br-product__price">
                <strong>
                    <span className="money">${price}</span>
                </strong>
                <del>
                    <span className="money">${strickedPrice}</span>
                </del>
            </span>
            <span className="spr-badge">
                <span className="spr-starrating spr-badge-starrating"><i className="spr-icon spr-icon-star-empty  starcolor" ></i><i className="spr-icon spr-icon-star-empty starcolor"></i><i className="spr-icon spr-icon-star-empty starcolor"></i><i className="spr-icon spr-icon-star-empty starcolor"></i><i className="spr-icon spr-icon-star-empty starcolor"></i></span>
            </span>
        </div>
    </div>
);

export default Product;