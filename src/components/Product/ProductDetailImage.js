import React, { Component } from "react";

class ProductDetailImage extends Component{

    state = {
        active: 0
    };
        
    activeImage(active){
        this.setState({active});
    }

    render(){
        let { images } = this.props;
        let { active } = this.state;
        return (
            <div className="br-product__media br-product-slide-vertical-image">
                <div className="br-product__image">
                    <div>
                        <div className="br-product__thumb">
                            <img className="engoj_img_main popimage" src={images[active]} alt="Bluisemod Bmet"  />
                        </div>
                    </div>
                </div>
                <div className="br-product-nav-wrapper">
                    <ul className="list-inline">
                    {
                        images.map((image, index) => <VerticalImage active={active === index ? true: false} image={image} key={index} onMouseEnter={() => this.activeImage(index)} />)
                    }
                    </ul>
                </div>
            </div>
        )
    }
}

const VerticalImage = ({image, onMouseEnter, active}) => (
    <li onMouseEnter={onMouseEnter} className={active ? "detailactive" : ""}>
        <div className="br-product__thumb engoj_img_variant">
            <img src={image} alt="Bluisemod Bmet" title="Bluisemod Bmet" />
        </div>
    </li> 
);

export default ProductDetailImage;