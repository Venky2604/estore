import React from "react"
import Cartquantity from "./Cart/CartQuantity";
import Slider from "./Slider";
import Product from "./Product/Product"

//images import 
import fashion01 from "../assets/images/fashion-01.jpg";
import fashion02 from "../assets/images/fashion-02.jpg";
import fashion03 from "../assets/images/fashion-03.jpg";
import fashion04 from "../assets/images/fashion-04.jpg";
import ProductDetailImage from "./Product/ProductDetailImage";

const ProductDetail = () => (
    <div className="m150">
        <section className="section-detail pb-30">
            <div className="container">
                <div className="br-product br-product-detail br-product-slide-vertical mt--90">
                    <div className="row">
                        <div className="col-lg-6">
                            <ProductDetailImage images={[fashion01, fashion02, fashion03, fashion04]} />
                        </div>
                        <div className="col-lg-6">
                            <div className="br-product-detail__container">
                                <div className="br-product-detail__header">
                                    <ol className="breadcrumb">
                                        <li>
                                            <a href="/" title="Back to the frontpage">Home</a>
                                        </li>
                                        <li className="active">Bluisemod Cmet</li>
                                    </ol>
                                </div>
                                <div className="br-product-detail__main">
                                    <h2 className="text-uppercase normal">Bluisemod Cmet</h2>
                                    <div className="mb-20">
                                        <span className="in-stock">In Stock</span>
                                    </div>
                                    <span className="br-product__price fz-20 enj-product-price">
                                        <span className="br-product__price">
                                            <span className="money" data-currency-usd="$88.00 USD" data-currency="USD">$88.00 USD</span>
                                        </span>
                                    </span>
                                    <div className="rating mb-15">
                                        <span className="spr-badge">
                                            <span className="spr-starrating spr-badge-starrating">
                                                
                                            </span>
                                            <span className="spr-badge-caption">No reviews</span>
                                        </span>
                                    </div>
                                    <div className="br-product__description desc">
                                        <p>Lorem ipsum dolor sit amet isse potenti Lorem ipsum dolor sit amet isse potenti.
                                            Vesquam ante aliquet lacusemper elit. Cras neque nulla, convallis non commodo
                                            et, euismod nonsese. At vero eos et accusamus et iusto...</p>
                                    </div>
                                    <form className="cart clearfix br-product__action">
                                        <div className="selector-wrapper">
                                            <select className="single-option-selector" data-option="option1" id="productSelect-option-0">
                                                <option value="Default Title">Default Title</option>
                                            </select>
                                        </div>
                                        <select name="id" id="productSelect" className="engoj-except-select2 product-single__variants">
                                            <option>Default Title - $88.00 USD</option>
                                        </select>
                                        <div className="quantity mb-20 engoc-quantity">
                                            <Cartquantity maxQuantity={9} quantity={1} />
                                        </div>
                                        <div id="ProductPrice" className="fz-16 mb-20">
                                            <label>Subtotal:</label>
                                            <span className="br-product__price engoj_price_main">
                                                <span className="money" data-currency-usd="$88.00 USD" data-currency="USD">$88.00 USD</span>
                                            </span>
                                        </div>
                                        <button className="enj-add-to-cart-btn btn btn-default btn-cart" type="submit" name="add" id="AddToCart" value="Add to Cart"
                                            style={{opacity: 1, marginRight: 10}}>
                                            <i className="fa icon-bag icons"></i>
                                            <span>Add to Cart</span>
                                        </button>
                                        <div className="cart-tool clearfix engoc-border-wistlist inline-block">
                                            <a className="add_to_wishlist wishlist  awe-button product-quick-whistlist btn" href="/account/login" data-toggle="tooltip" data-placement="top"
                                                title="" data-original-title="Add to Wishlist">
                                                <span className="sr-only">Add to Wishlist</span>
                                                <i className="icon-heart icons"></i>
                                            </a>
                                        </div>
                                    </form>
                                </div>
                                <div className="overflow-hidden">
                                    Category:
                                    <ul className="br-category">

                                        <li>
                                            <a href="/collections/fashion">Fashion</a>
                                        </li>

                                        <li>
                                            <a href="/collections/fashion-slider">Fashion Slider</a>
                                        </li>

                                    </ul>
                                    - Tags:
                                    <ul className="br-tags">

                                        <li>
                                            <a href="/collections/all?constraint=0-100">$0-$100</a>
                                        </li>

                                        <li>
                                            <a href="/collections/all?constraint=fashion">fashion</a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div className="br-site-social pt-10 pb-10">
            <div className="container">
                <ul className="nav-social">
                    <li>
                        <a className="fz-18">
                            <i className="fa fa-google-plus"></i>
                            <span>Google Plus</span>
                        </a>
                    </li>
                    <li>
                        <a className="fz-18">
                            <i className="fa fa-facebook"></i>
                            <span>Facebook</span>
                        </a>
                    </li>
                    <li>
                        <a className="fz-18">
                            <i className="fa fa-pinterest"></i>
                            <span>Pinterest</span>
                        </a>

                    </li>
                    <li>
                        <a className="fz-18">
                            <i className="fa fa-twitter"></i>
                            <span>Twitter</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div className="pt-80 pb-60 engoc_no_rtl">
            <div className="container">
                <h2 className="br-title text-center">Related products</h2>
                <Slider 
                    slideCount={4}
                    slideWidth={25}
                    widthMetric={`%`}
                    containerClasses={["row", "br-slick-white", "related-slider"]}
                >
                    <div className="col-md-3 inline-block float-none">
                        <Product 
                            title="Product 01" 
                            images={[fashion01, fashion02] }
                            price={10920}
                            strickedPrice={20920}
                        />
                    </div>
                    <div className="col-md-3 inline-block float-none">
                        <Product 
                            title="Product 01" 
                            images={[fashion03, fashion02] }
                            price={10920}
                            strickedPrice={20920}
                        />
                    </div>
                    <div className="col-md-3 inline-block float-none">
                        <Product 
                            title="Product 01" 
                            images={[fashion02, fashion01] }
                            price={10920}
                            strickedPrice={20920}
                        />
                    </div>
                    <div className="col-md-3 inline-block float-none">
                        <Product 
                            title="Product 01" 
                            images={[fashion04, fashion02] }
                            price={10920}
                            strickedPrice={20920}
                        />
                    </div>
                    <div className="col-md-3 inline-block float-none">
                        <Product 
                            title="Product 01" 
                            images={[fashion01, fashion04] }
                            price={10920}
                            strickedPrice={20920}
                        />
                    </div>
                    <div className="col-md-3 inline-block float-none">
                        <Product 
                            title="Product 01" 
                            images={[fashion03, fashion01] }
                            price={10920}
                            strickedPrice={20920}
                        />
                    </div>
                </Slider>
            </div>
        </div>
    </div>
);

export default ProductDetail;