import React from 'react';
import logo from "../assets/images/flatly_logo.png";
import { Link } from "react-router-dom";
import Mobilemenu from './MobileMenu';

const Header = () => (
    <div>
        <div className="shopify-section desktopmenu stickyheader">
            <header className="site-header">
                <nav className="awemenu-nav awemenu-nav--black awemenu-fadeup engoc-header-nav-s1">
                    <div className="container-fluid nav-main-container">
                        <div className="awemenu-logo engoc-h1-maxwidth-logo">
                            <Link to="/">
                                <img src={ logo } alt="logo" />
                            </Link>
                        </div>
                        <div className="nav-container">
                            <div className="navbar-icons">
                                <div className="dropdown dropdown-animation engoc-dropdown-search">
                                    <a  data-toggle="dropdown" role="button">
                                        <i className="icon-magnifier icons" aria-hidden="true"></i>
                                    </a>
                                    <div className="dropdown-menu dropdown-search">
                                        <div className="form-search">
                                            <form className="searchform">
                                                <label className="sr-only">search</label>
                                                <input type="text" id="engo_autocomplate" value="" autoComplete="off" placeholder="Search our store" aria-label="Search our store" name="q" />
                                                <button type="button" className="submit"><i className="icon-magnifier icons" aria-hidden="true"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div className="dropdown-right">
                                    <div className="dropdown dropdown-animation">
                                        <Link to="/cart" data-toggle="dropdown" role="button">
                                            <i className="icon-bag icons" aria-hidden="true"></i>
                                            <span id="CartCount" className="count engoc-cart-count">0</span>
                                        </Link>
                                        <div className="dropdown-menu dropdown-cart">
                                            <div className="list-product-mini" id="CartContainer">
                                                <div className="text-center">
                                                    <p>Your shopping bag is empty</p>
                                                </div>
                                                <div className="text-center">
                                                    <Link to="/shop" className="btn btn-default">Go to the shop</Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="dropdown dropdown-animation">
                                        <a href="/cart" data-toggle="dropdown" role="button">
                                            <i className="icon-heart icons" aria-hidden="true"></i>
                                        </a>
                                        <div className="dropdown-menu dropdown-wishlist">
                                            <div className="list-product-mini">
                                                <p className="text-center">
                                                    <span>Please login: </span>
                                                    <a>Login</a>
                                                    <span> - </span>
                                                    <a>Register now?</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="dropdown dropdown-animation">
                                        <a  data-toggle="dropdown" role="button">
                                            <i className="icon-user icons" aria-hidden="true"></i>
                                        </a>
                                        <div className="dropdown-menu dropdown-account">
                                            <a className="account-link br-product__name">Register</a>
                                            <br />
                                            <a className="account-link br-product__name">Log in</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul className="awemenu list-inline">
                                <li className="awemenu-item">
                                    <Link to="/">Home</Link>
                                </li>
                                <li className="awemenu-item">
                                    <Link to="/shop">Shop</Link>
                                </li>
                                <li className="awemenu-item">
                                    <Link to="/shop">Men</Link>
                                </li>
                                <li className="awemenu-item">
                                    <Link to="/shop">Women</Link>
                                </li>
                                <li className="awemenu-item">
                                    <Link to="/shop">Kids</Link>
                                </li>
                            </ul>      
                        </div>
                    </div>
                </nav>
            </header>
        </div>
        <Mobilemenu />
    </div>
);

export default Header;