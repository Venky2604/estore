import React from "react";
import Product from "./Product/Product";
import { Link } from "react-router-dom";

//images import 
import fashion01 from "../assets/images/fashion-01.jpg";
import fashion02 from "../assets/images/fashion-02.jpg";
import fashion03 from "../assets/images/fashion-03.jpg";
import fashion04 from "../assets/images/fashion-04.jpg";

const ShopPage = () => (
	<div id="shopify-section-collection-template" className="shopify-section">
		<div className="page-title page-title--classic  mb-0 bg-12"  data-section-id="collection-template" data-section-type="collection-template">
			<div className="container">
				<div className="page-title__main">
					<div className="row">
						<div className="col-md-6 col-sm-6">
						<h1 className="text-uppercase mb-5">Products</h1>
						</div>
						<div className="col-md-6 col-sm-6">
						<ol className="breadcrumb">
							<li>
								<Link to="/" title="Back to the frontpage">Home</Link>
							</li>
							<li className="active">Products</li>
						</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
		<section>
		<div className="container">
			<div className="br-filter-wrapper">
			<div className="overflow-hidden">
				<div className="br-right">
				<form className="products-sortby" method="get" action="#">
					<div className="select-icon br-filter-group br-filter-group--show">
					<span>Show</span>
					<input type="hidden" name="engoj_current_page_size" className="engoj_current_page_size" value="12" />
					<select name="showby" id="showby" className="engoj-collection-show">
						<option value="12"> 12 </option>
						<option value="24"> 24 </option>
						<option value="48"> 48 </option>
					</select>
					</div>
					<div className="select-icon br-filter-group br-filter-group--sort">
					<span>Sort By</span>
					<select name="SortBy" id="SortBy" className="engoj-collection-sorting">
						<option value="manual">Featured</option>
						<option value="best-selling">Best Selling</option>
						<option value="title-ascending">Alphabetically, A-Z</option>
						<option value="title-descending">Alphabetically, Z-A</option>
						<option value="price-ascending">Price, low to high</option>
						<option value="price-descending">Price, high to low</option>
						<option value="created-descending">Date, new to old</option>
						<option value="created-ascending">Date, old to new</option>
					</select>
					</div>
				</form>
				</div>
			</div>
			</div>
		</div>
		<div className="container">
			<div className="row">
				<div className="grid-uniform col-md-9 col-md-push-3">
					<div className="engoc-row-equal">
						<div className="col-md-4 col-sm-6 col-tn-6">
							<Product 
                                title="Product 01" 
                                images={[fashion01, fashion02] }
                                price={10920}
                                strickedPrice={20920}
                            />
						</div>
						<div className="col-md-4 col-sm-6 col-tn-6">
							<Product 
                                title="Product 01" 
                                images={[fashion02, fashion03] }
                                price={10920}
                                strickedPrice={20920}
                            />
						</div>
						<div className="col-md-4 col-sm-6 col-tn-6">
							<Product 
                                title="Product 01" 
                                images={[fashion03, fashion04] }
                                price={10920}
                                strickedPrice={20920}
                            />
						</div>
						<div className="col-md-4 col-sm-6 col-tn-6">
							<Product 
                                title="Product 01" 
                                images={[fashion04, fashion01] }
                                price={10920}
                                strickedPrice={20920}
                            />
						</div>
						<div className="col-md-4 col-sm-6 col-tn-6">
							<Product 
                                title="Product 01" 
                                images={[fashion02, fashion01] }
                                price={10920}
                                strickedPrice={20920}
                            />
						</div>
						<div className="col-md-4 col-sm-6 col-tn-6">
							<Product 
                                title="Product 01" 
                                images={[fashion03, fashion04] }
                                price={10920}
                                strickedPrice={20920}
                            />
						</div>
						<div className="col-md-4 col-sm-6 col-tn-6">
							<Product 
                                title="Product 01" 
                                images={[fashion02, fashion03] }
                                price={10920}
                                strickedPrice={20920}
                            />
						</div>
						<div className="col-md-4 col-sm-6 col-tn-6">
							<Product 
                                title="Product 01" 
                                images={[fashion03, fashion04] }
                                price={10920}
                                strickedPrice={20920}
                            />
						</div>
						<div className="col-md-4 col-sm-6 col-tn-6">
							<Product 
                                title="Product 01" 
                                images={[fashion04, fashion01] }
                                price={10920}
                                strickedPrice={20920}
                            />
						</div>
						<div className="col-md-4 col-sm-6 col-tn-6">
							<Product 
                                title="Product 01" 
                                images={[fashion02, fashion03] }
                                price={10920}
                                strickedPrice={20920}
                            />
						</div>
						<div className="col-md-4 col-sm-6 col-tn-6">
							<Product 
                                title="Product 01" 
                                images={[fashion03, fashion04] }
                                price={10920}
                                strickedPrice={20920}
                            />
						</div>
						<div className="col-md-4 col-sm-6 col-tn-6">
							<Product 
                                title="Product 01" 
                                images={[fashion04, fashion01] }
                                price={10920}
                                strickedPrice={20920}
                            />
						</div>
					</div>
					<div className="text-center br-pagination mb-35">
						<div className="pagination engoj-pagination-collection">
							<span className="prev"><a title=""><i className="fa fa-arrow-left faright"></i></a></span>
							<span className="page pageactive"><a title="">1</a></span> 
							<span className="page"><a title="">2</a></span> 
							<span className="page"><a title="">3</a></span> 
							<span className="page"><a title="">4</a></span> 
							<span className="next"><a title=""><i className="fa fa-arrow-right faright"></i></a></span>
						</div>
        			</div>
				</div>
				<div className="col-md-3 col-md-pull-9">
					<aside className="sidebar widget-area collection-sidebar pb-40">
					<section className="widget widget_categories">
						<h2 className="widget-title">All Categories</h2>
						<ul className="engoj-ajax-widget">
						<li>
							<a>Bikini
							<span>(24)</span>
							</a>
						</li>
						<li>
							<a>Fashion
							<span>(15)</span>
							</a>
						</li>
						<li>
							<a>Fashion Slider
							<span>(15)</span>
							</a>
						</li>
						</ul>
					</section>
					<section className="widget widget_categories widget-container widget_product_categories filter-block filter-custom filter-tag color">
						<h2 className="widget-title">Color</h2>
						<ul className=" colors">
						<li className="li-color">
							<input type="checkbox" value="steelblue" />
							<a title="SteelBlue" className="">SteelBlue</a>
						</li>
						<li className="li-color">
							<input type="checkbox" value="tan" />
							<a title="Tan" className="">Tan</a>
						</li>
						<li className="li-color">
							<input type="checkbox" value="violet" />
							<a title="Violet" className="">Violet</a>
						</li>
						<li className="li-color">
							<input type="checkbox" value="yellowgreen" />
							<a title="YellowGreen" className="">YellowGreen</a>
						</li>
						<li className="li-color">
							<input type="checkbox" value="lemonchiffon" />
							<a title="LemonChiffon" className="">LemonChiffon</a>
						</li>
						<li className="li-color">
							<input type="checkbox" value="hotpink" />
							<a title="HotPink" className="">HotPink</a>
						</li>
						<li className="li-color">
							<input type="checkbox" value="palegreen" />
							<a title="PaleGreen" className="">PaleGreen</a>
						</li>
						<li className="li-color">
							<input type="checkbox" value="cadetblue" />
							<a title="CadetBlue" className="">CadetBlue</a>
						</li>
						<li className="li-color">
							<input type="checkbox" value="palevioletred" />
							<a title="PaleVioletRed" className="">PaleVioletRed</a>
						</li>
						<li className="li-color">
							<input type="checkbox" value="mediumspringgreen" />
							<a title="MediumSpringGreen" className="">MediumSpringGreen</a>
						</li>
						</ul>
					</section>
					<section className="widget widget_categories widget-container widget_product_categories filter-block filter-custom filter-tag size">
						<h2 className="widget-title">Size Option
						
						</h2>
						<div className="filter-content">
						<ul className="sizes">
							<li className="li-size">
							<input type="checkbox" value="s" />
							<a title="S" className="">S</a>
							</li>
							<li className="li-size">
							<input type="checkbox" value="m" />
							<a title="M" className="">M</a>
							</li>
							<li className="li-size">
							<input type="checkbox" value="l" />
							<a title="L" className="">L</a>
							</li>
							<li className="li-size">
							<input type="checkbox" value="xl" />
							<a title="XL" className="">XL</a>
							</li>
						</ul>
						</div>
					</section>
					<section className="widget widget_categories widget-container widget_product_categories filter-block filter-custom filter-tag">
						<h2 className="widget-title">Price Filter
						</h2>
						<ul>
						<li className="li-price">
							<input type="checkbox" value="48-100" />
							<a title="48$-100$" className="">48$-100$</a>
						</li>
						<li className="li-price">
							<input type="checkbox" value="100-200" />
							<a title="100$-200$" className="">100$-200$</a>
						</li>
						<li className="li-price">
							<input type="checkbox" value="200-300" />
							<a title="200$-300$" className="">200$-300$</a>
						</li>
						<li className="li-price">
							<input type="checkbox" value="300-400" />
							<a title="300$-400$" className="">300$-400$</a>
						</li>
						</ul>
					</section>
					<section className="widget widget_categories">
						<h2 className="widget-title">Brand</h2>
						<ul className="engoj-ajax-widget">
						<li>
							<a title="Flatly - Multi Store Responsive Shopify Theme">Flatly - Multi Store Responsive Shopify Theme</a>
						</li>
						</ul>
					</section>
					</aside>
				</div>
			</div>
		</div>
		</section>
	</div>
);

export default ShopPage;
