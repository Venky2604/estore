import React from 'react';
import Header from "./Header";
import Footer from './Footer';
import ScrollToTop from './ScrollToTop';

const CommonContainer = ({ children }) => (
    <ScrollToTop>
        <Header />
        <main className="site-main">
            {children}
        </main>
        <Footer />
    </ScrollToTop>
);

export default CommonContainer;