import React from 'react';
import logo from "../assets/images/flatly_logo.png";

const Footer = () => (
<div id="shopify-section-footer" className="shopify-section">
    <footer className="site-footer engoc_footer_v4">
        <div className="br-footer-content">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-4 col-sm-12">
                        <div className="br-site-contact">
                            <a href="/" title="Contact Us">Contact Us</a>

                            <a href="/collections" title="Collection">Collection</a>

                            <a href="/" title="Privacy Policy">Privacy Policy</a>

                            <a href="/" title="Site Map">Site Map</a>
                        </div>
                    </div>
                    <div className="col-md-4 text-center">
                        <div className="br-logo-wrapper">
                            <a className="br-logo" style={{ maxWidth: `100px`}}>
                                <img src={ logo } alt="Logo" />
                            </a>
                            <p>
                                <span>Copyright 2018 &copy;
                                    <a title="ShopiLaunch" target="_blank">ShopiLaunch.</a>
                                    <br />
                                    <small>
                                        <a>Powered by Shopify</a>
                                    </small>
                                </span>
                            </p>


                        </div>
                    </div>
                    <div className="col-md-4 col-sm-12">
                        <section className="widget widget-text text-right engoc-payment">
                            <div className="br-payment">
                                <a >payment</a>
                                <a >
                                    <i className="fa fa-cc-visa" aria-hidden="true"></i>
                                </a>
                                <a >
                                    <i className="fa fa-cc-mastercard" aria-hidden="true"></i>
                                </a>
                                <a >
                                    <i className="fa fa-cc-paypal" aria-hidden="true"></i>
                                </a>
                                <a >
                                    <i className="fa fa-cc-discover" aria-hidden="true"></i>
                                </a>

                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
);

export default Footer;