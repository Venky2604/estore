import React from "react";

import fashion01 from "../assets/images/fashion-01.jpg";
import fashion02 from "../assets/images/fashion-02.jpg";
import Cartquantity from "./Cart/CartQuantity";

const CartPage = () => (
	<section className="section table-cart">
		<div className="container">
			<form  method="post" noValidate className="cart table-wrap table-cart-form">
			<div className="table-responsive">
				<table className="cart-table table--responsive shop_table cart table table-hover table_cart carttable">
				<tbody>
					<tr className="cart__row table__section cart_item">
						<td data-label="Product" className="product-thumbnail">
							<a className="cart__image attachment-shop_thumbnail wp-post-image">
							<img src={fashion01} alt="Bluisemod Bmet" className="img-responsive cartimage" />
							</a>
						</td>
						<td className="product-name">
							<a className="h6">
							Bluisemod Bmet
							</a>
						</td>
						<td data-label="Price" className="product-price">
							<span className="h6 amount br-product__price">
							<span className="money">$68.00</span>
							</span>
						</td>
						<td data-label="Quantity" className="product-quantity engoc-cart-qty">
							<Cartquantity maxQuantity={9} quantity={1} />
						</td>
						<td data-label="Total" className="text-right product-subtotal">
							<span className="h6 amount br-product__price">
							<span className="money">$68.00</span>
							</span>
						</td>
						<td className="product-remove">
							<a className="cart__remove" title="Remove">
							<i className="fa fa-times"></i>
							</a>
						</td>
					</tr>
					<tr className="cart__row table__section cart_item">
						<td data-label="Product" className="product-thumbnail">
							<a className="cart__image attachment-shop_thumbnail wp-post-image">
								<img src={fashion02} alt="Bluisemod Amet" className="img-responsive cartimage" />
							</a>
						</td>
						<td className="product-name">
							<a className="h6">
							Bluisemod Amet
							</a>
						</td>
						<td data-label="Price" className="product-price">
							<span className="h6 amount br-product__price">
							<span className="money">$69.00</span>
							</span>
						</td>
						<td data-label="Quantity" className="product-quantity engoc-cart-qty">
							<Cartquantity maxQuantity={9} quantity={1} />
						</td>
						<td data-label="Total" className="text-right product-subtotal">
							<span className="h6 amount br-product__price">
							<span className="money">$69.00</span>
							</span>
						</td>
						<td className="product-remove">
							<a className="cart__remove" title="Remove">
							<i className="fa fa-times"></i>
							</a>
						</td>
					</tr>
				</tbody>
				</table>
			</div>
			<div className="text-center">
				<h5 className="text-uppercase">cart totals</h5>
				<h2 className="total-price mb-25">
				<span className="money">$137.00</span>
				</h2>
				<input type="submit" name="update" className="btn btn-lg btn-outline btn-default btn--secondary update-cart" value="Update Cart"
				/>
				<input type="submit" name="checkout" className="btn btn-lg btn-default" value="Check Out" />
			</div>
			</form>
		</div>
	</section>
);

export default CartPage;