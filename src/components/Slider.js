import React, { Component } from 'react';

class Slider extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            active: 0,
            translateX: 0
        };
        this.nextSlide = this.nextSlide.bind(this);
        this.prevSlide = this.prevSlide.bind(this);
    }    

    prevSlide(){
        this.setState(function(prevState, props){
            let { translateX, active } = prevState;
            let { slideWidth } = props;
            if( active !== 0){
                return {
                    active: active - 1,
                    translateX: translateX + slideWidth 
                }
            }
        })
    }

    nextSlide(){
        this.setState(function(prevState, props){
            let { translateX, active } = prevState;
            let { slideWidth, children, slideCount } = props;
            if( active !== (children.length - slideCount) ){
                return {
                    active: active + 1,
                    translateX: translateX - slideWidth 
                }
            }
        })
    }

    render() {
        let { translateX } = this.state;
        let { children, widthMetric, slideCount} = this.props;
        return (
            <div className={this.props.containerClasses.join(" ") + " slidercontainer"}
            >
                { children.length > slideCount &&
                    <button type="button" className="slick-prev slick-arrow" 
                    onClick={this.prevSlide}
                    >Previous</button>
                }
                <div 
                    className="slidertransition" 
                    style={{transform: `translateX(${translateX}${widthMetric })`}}
                >
                    {
                        children
                    }
                </div>
                { children.length > slideCount &&
                    <button type="button" data-role="none" className="slick-next slick-arrow" aria-label="Next"  aria-disabled="false" onClick={this.nextSlide}>Next</button>
                }
            </div>
        );
    }
}

export default Slider;