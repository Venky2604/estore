import React from 'react';
import ReactDOM from 'react-dom';
import "./assets/css/main.css" ; 
import "./assets/css/style.scss.css" ;
import "./assets/css/spricons.css" ;
import "./assets/css/font-awesome.min.css" ; 
import "./assets/css/simple-line-icons.css" ; 
import "./assets/css/slick.css" ; 
import "./assets/css/awemenu.css" ; 
import "./assets/css/engo-customize.scss.css" ; 
import "./assets/css/timber.scss.css" ; 
import "./assets/css/umbg.css" ; 
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
